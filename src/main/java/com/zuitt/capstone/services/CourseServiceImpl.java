package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // Add course
    public void createCourse(String stringToken, Course course) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //Retrieve all courses
    public Iterable<Course> getCourses() {
        return courseRepository.findAll();
    }

    // Update course
    public ResponseEntity updateCourse(int id, String stringToken,  Course course) {
        Course courseForUpdating = courseRepository.findById(id).get();
        String courseAuthorName = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);
            return new ResponseEntity<>("Course updated successfully!", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Delete course
    public ResponseEntity deleteCourse(int id, String stringToken) {
        Course courseForDeleting = courseRepository.findById(id).get();
        String courseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully!", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Retrieve user course
    public Iterable<Course> getMyCourses(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }
}
