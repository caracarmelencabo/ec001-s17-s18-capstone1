package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    void createCourse(String stringToken, Course course);

    Iterable<Course> getCourses();

    ResponseEntity deleteCourse(int id, String stringToken);

    ResponseEntity updateCourse(int id, String stringToken, Course course);

    Iterable<Course> getMyCourses(String stringToken);

}
