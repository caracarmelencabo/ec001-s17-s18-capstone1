package com.zuitt.capstone.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

public class JwtAuthenticate implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = 161361350241257696L;

    @Override
    //This will handle the authentication failures
    // AuthenticationException is a class in Spring Security that contains all exceptions related to an Authentication object being invalid
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {

        //This will send an HTTP error response with a 401 status code with an "Unauthorized" message to the client
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }
}
