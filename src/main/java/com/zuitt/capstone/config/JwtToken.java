package com.zuitt.capstone.config;

import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {
    //taken from application.properties
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = -4786853465489526691L;

    //Time duration in seconds that the token can be used
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60; //5 hrs

    //This code generates a JWT by setting the claims, subject, issued-at time, expiration time, signing the JWT using a secret key and then compacting the JWT builder object to generate the final JWT String
    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000)).signWith(SignatureAlgorithm.HS512,secret).compact();
    }

    //This code generates a JWT token for a user by creating a map of claims, setting a "user" claim to the user's ID and then generating the token using the doGenerateToken method
    public String generateToken(UserDetails userDetails) {

        Map<String, Object> claims = new HashMap<>();

        //Retrieves the user information from the userRepository by using the username from the UserDetails object
        User user = userRepository.findByUsername(userDetails.getUsername());

        //This adds a claim in the "claims" map by setting a key to "user" and value of "user's ID. This claim is added to JWT that can be used to identify the user in the server
        claims.put("user", user.getId());

        //This invokes the doGenerateToken method and pass the "claims" and the user's username from the UserDetails object
        return doGenerateToken(claims, userDetails.getUsername());
    }

    //This code validates the token by extracting the username from the token
    public Boolean validateToken(String token, UserDetails userDetails){

        //Extract the username from the token and store it in a variable named token
        final String username = getUsernameFromToken(token);

        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        //This calls "getAllClaimsFromToken" method to retrieve all the claims from the JWT token
        final Claims claims = getAllClaimsFromToken(token);

        //This extracts the specific claim value by using the "apply()" method
        return claimsResolver.apply(claims);
    }

    //This extracts all the claims from the token by taking the "token" String as an input
    private Claims getAllClaimsFromToken(String token) {

        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //This is used to extract the subject(username) from the JWT token
    public String getUsernameFromToken(String token){

        String claim = getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

    //This is used to extract the expiration date of the token
    public Date getExpirationDateFromToken(String token){

        // "Claims::getExpiration" method reference is used to extract the expiration time claim from the JWT token.
        return getClaimFromToken(token, Claims::getExpiration);
    }

    //This is used to check if the JWT token has expired
    private Boolean isTokenExpired(String token){


        // getExpirationDateFromToken() invokes the "getExpirationDateFromToken()" method with the "token" as parameter to retrieve expiration date which is placed inside the "expiration" variable
        final Date expiration = getExpirationDateFromToken(token);

        return expiration.before(new Date());
    }
}
