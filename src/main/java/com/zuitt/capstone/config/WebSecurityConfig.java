package com.zuitt.capstone.config;

import com.zuitt.capstone.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticate jwtAuthenticate;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public JwtAuthenticate jwtAuthenticateEntryPointBean() throws Exception{
        return new JwtAuthenticate();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    //Routes that will not require JWT tokens
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.cors().and().csrf().disable()
                .authorizeRequests()

                .antMatchers("/authenticate").permitAll()

                .antMatchers("/users/register").permitAll()

                //allows the "/courses" endpoint to be accessible via GET method without requiring any authentication
                .antMatchers(HttpMethod.GET, "/courses").permitAll()

                //allows unrestricted access to HTTP OPTIONS requests made to any URL in the application, regardless of the path, without requiring any authentication
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                //Specifies that any other requests not matching the above rules should be authenticated, meaning it requires proper authentication to be accessed
                .anyRequest().authenticated().and()

                //This configures the authentication entry point for handling authentication failures
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticate).and()

                //This is used to specify that the application should not create or use any sessions as it is following a statelesss authentication approach using JWT
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
