package com.zuitt.capstone.models;

import java.io.Serializable;
public class JwtResponse implements Serializable{

    private static final long serialVersionUID = -2328257906101597369L;

    //Properties
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    //Getters and Setters

    public String getToken(){
        return this.jwttoken;
    }
}
