package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    // Add course
    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value="Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken, course);
        return new ResponseEntity<>("Course created successfully!", HttpStatus.CREATED);
    }

    // Retrieve all courses
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses() {
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    // Update course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable int courseid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course) {
        return courseService.updateCourse(courseid, stringToken, course);
    }

    // Delete course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseid, @RequestHeader(value="Authorization") String stringToken) {
        return courseService.deleteCourse(courseid, stringToken);
    }

    // Retrieve user course
    @RequestMapping(value = "/myCourses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }


}
